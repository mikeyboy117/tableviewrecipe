//
//  AppDelegate.h
//  TableViewRecipe2
//
//  Created by Michael Childs on 5/19/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

