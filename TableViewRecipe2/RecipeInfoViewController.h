//
//  RecipeInfoViewController.h
//  TableViewRecipe2
//
//  Created by Michael Childs on 5/20/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeInfoViewController : UIViewController

@property (nonatomic, strong) NSDictionary* recipeInfoDictionary;

@end
