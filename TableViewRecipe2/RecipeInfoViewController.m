//
//  RecipeInfoViewController.m
//  TableViewRecipe2
//
//  Created by Michael Childs on 5/20/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import "RecipeInfoViewController.h"

@interface RecipeInfoViewController ()

@end

@implementation RecipeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor grayColor]];
    
    UIImageView* recipeImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, 250)];
    recipeImg.image = [UIImage imageNamed:[self.recipeInfoDictionary objectForKey:@"recipeImg"]];
    recipeImg.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:recipeImg];
    
    
    UITextView* recipeText = [[UITextView alloc]initWithFrame:CGRectMake(10, recipeImg.frame.size.height + 75, self.view.frame.size.width, self.view.frame.size.height/2)];
    recipeText.text = [self.recipeInfoDictionary objectForKey:@"recipeText"];
    recipeText.textColor = [UIColor whiteColor];
    recipeText.backgroundColor = [UIColor grayColor];
    recipeText.editable = NO;
    [self.view addSubview:recipeText];
    
    self.navigationController.topViewController.title =[self.recipeInfoDictionary objectForKey:@"recipeName"]; // display recipe name within navigationBar
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
