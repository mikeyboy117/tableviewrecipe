//
//  ViewController.m
//  TableViewRecipe2
//
//  Created by Michael Childs on 5/19/15.
//  Copyright (c) 2015 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.topViewController.title = @"Recipes";
    
    
    recipeInfoArray = @[
                        
                        @{
                            @"recipeName":@"Chana Masala",
                            @"recipeImg":@"chanaMasala",
                            @"recipeText":@"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired."
                        },
                        
                        @{
                            @"recipeName":@"Tika Masala",
                            @"recipeImg":@"tikaMasala",
                            @"recipeText":@"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired."
                        },
                        
                        @{
                            @"recipeName":@"Black Beans & Rice",
                            @"recipeImg":@"bbRice",
                            @"recipeText":@"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired."
                        },
                        
                        @{
                            @"recipeName":@"Chicken & Curry",
                            @"recipeImg":@"chickenCurry",
                            @"recipeText":@"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired."
                        },
                        
                        @{
                            @"recipeName":@"Chocolate Chip Cookies",
                            @"recipeImg":@"ccCookies.jpg",
                            @"recipeText":@"This simplified version of an Indian favorite is a delightful way to showcase tasty chickpeas.\n\ningredients\n1 tablespoon olive oil\n1 large onion, chopped\n2 to 3 garlic cloves, minced\nTwo 15- to 16-ounce cans chickpeas, drained and rinsed\n1 to 2 teaspoons garam masala or good-quality curry powder n1/2 teaspoon turmeric\n2 teaspoons grated fresh or jarred ginger\n2 large tomatoes, diced\n1 tablespoon lemon juice\n1/4 cup minced fresh cilantro, or to taste\nSalt to taste\nHot cooked grain (rice, quinoa, or couscous), optional preparation\n\n1. Heat the oil in a wide skillet. Add the onion and sauté until translucent. Add the garlic and continue to sauté until the onion is golden.\n\n2. Add the chickpeas, garam masala, turmeric, ginger, tomatoes, lemon juice, and about 1/4 cup water. Bring to a simmer, then cook over medium-low heat for 10 minutes, stirring frequently. This should be moist and stewlike, but not soupy; add a little more water, if needed.\n\n3. Stir in the cilantro and season with salt. Serve on its own in shallow bowls or over a hot cooked grain, if desired."
                        }
                        
                        ];
    UITableView* recipeTable = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    recipeTable.delegate = self;
    recipeTable.dataSource = self;
    [self.view addSubview:recipeTable];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return recipeInfoArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary* recipeDictionary = recipeInfoArray[indexPath.row];
    
    cell.textLabel.text = [recipeDictionary objectForKey:@"recipeName"];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* infoDictionary = recipeInfoArray[indexPath.row];
    
    RecipeInfoViewController* info = [RecipeInfoViewController new];
    info.recipeInfoDictionary = infoDictionary;
    [self.navigationController pushViewController:info animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
